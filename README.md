# Spring Content Negotiator

A library to make it easier to perform content negotiation within Spring.

As noted in my articles below, this isn't straightforward to do, and I wanted to make it easier to utilise without as much plumbing in your project:

- [_Content Negotiation with `ControllerAdvice` and `ExceptionHandlers` in Spring (Boot)_](https://www.jvt.me/posts/2022/01/18/spring-negotiate-exception-handler/)
- [_Content Negotiation with Servlet `Filter` in Spring (Boot)_](https://www.jvt.me/posts/2022/01/18/spring-negotiate-servlet-filter/)

Please see the `examples` package under `src/test/java/` to see example use cases.

This library is released to Maven Central under the groupId `me.jvt.spring`, and is licensed under the Apache 2.0 License.

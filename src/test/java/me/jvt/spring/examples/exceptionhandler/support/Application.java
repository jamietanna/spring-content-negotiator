package me.jvt.spring.examples.exceptionhandler.support;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {}

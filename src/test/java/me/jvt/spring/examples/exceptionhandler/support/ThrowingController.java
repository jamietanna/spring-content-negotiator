package me.jvt.spring.examples.exceptionhandler.support;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThrowingController {
  @GetMapping(
      value = "/endpoint",
      produces = {"*/*"})
  public void throwing() throws BusinessException {
    throw new BusinessException();
  }
}

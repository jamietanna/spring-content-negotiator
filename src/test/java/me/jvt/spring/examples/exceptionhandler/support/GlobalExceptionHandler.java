package me.jvt.spring.examples.exceptionhandler.support;

import me.jvt.spring.ContentNegotiator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

  private final ContentNegotiator contentNegotiator;

  public GlobalExceptionHandler(ContentNegotiator contentNegotiator) {
    this.contentNegotiator = contentNegotiator;
  }

  @ExceptionHandler(BusinessException.class)
  public ResponseEntity<Object> handleBusinessException(BusinessException e, WebRequest request) {
    MediaType resolved;
    try {
      resolved = contentNegotiator.negotiate(request);
    } catch (HttpMediaTypeNotAcceptableException ex) {
      return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
    }

    if (resolved.equals(MediaType.TEXT_PLAIN)) {
      return ResponseEntity.status(HttpStatus.BAD_GATEWAY).contentType(resolved).build();
    } else if (resolved.equals(MediaType.APPLICATION_JSON)) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(resolved).build();
    } else {
      // This is a case where we've mis-implemented the negotiating handler cases
      throw new IllegalStateException(
          "`GlobalExceptionHandler#handleBusinessException` does not appear to have a case for the MediaType `%s`, which is very likely an implementation issue, as the `ContentNegotiator` was configured to allow it");
    }
  }
}

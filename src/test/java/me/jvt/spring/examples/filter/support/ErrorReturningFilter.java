package me.jvt.spring.examples.filter.support;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.spring.ContentNegotiator;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class ErrorReturningFilter extends OncePerRequestFilter {

  private final ContentNegotiator contentNegotiator;

  public ErrorReturningFilter(ContentNegotiator contentNegotiator) {
    this.contentNegotiator = contentNegotiator;
  }

  @Override
  protected void doFilterInternal(
      @NonNull HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain filterChain) {
    MediaType resolved;
    try {
      resolved = contentNegotiator.negotiate(request);
    } catch (HttpMediaTypeNotAcceptableException ex) {
      response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
      return;
    }

    if (resolved.equals(MediaType.TEXT_PLAIN)) {
      response.setStatus(HttpServletResponse.SC_BAD_GATEWAY);
      response.setContentType(resolved.toString());
    } else if (resolved.equals(MediaType.APPLICATION_JSON)) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      response.setContentType(resolved.toString());
    } else {
      // This is a case where we've mis-implemented the negotiating handler cases
      throw new IllegalStateException(
          "The `ErrorReturningFilter` does not appear to have a case for the MediaType `%s`, which is very likely an implementation issue, as the `ContentNegotiator` was configured to allow it");
    }
  }
}

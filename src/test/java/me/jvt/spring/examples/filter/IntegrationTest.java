package me.jvt.spring.examples.filter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import me.jvt.spring.examples.filter.support.Application;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
class IntegrationTest {
  @Autowired private MockMvc mockMvc;

  @Test
  void returnsTextPlainWhenRequested() throws Exception {
    mockMvc
        .perform(get("/endpoint").accept("text/plain"))
        .andExpect(status().isBadGateway())
        .andExpect(header().string("content-type", "text/plain"));
  }

  @Test
  void returnsJsonWhenRequested() throws Exception {
    mockMvc
        .perform(get("/endpoint").accept("application/json"))
        .andExpect(status().isBadRequest())
        .andExpect(header().string("content-type", "application/json"));
  }

  @Test
  void returnsNotAcceptableWhenNotSupported() throws Exception {
    mockMvc
        .perform(get("/endpoint").accept("application/pdf"))
        .andExpect(status().isNotAcceptable());
  }
}

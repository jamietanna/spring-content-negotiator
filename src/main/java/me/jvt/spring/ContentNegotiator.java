package me.jvt.spring;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import me.jvt.contentnegotiation.ContentTypeNegotiator;
import me.jvt.contentnegotiation.NotAcceptableException;
import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

/**
 * A utility to perform content-type negotiation within Spring applications, for instance when using
 * a Spring {@link OncePerRequestFilter} or a {@link ExceptionHandler}.
 */
public class ContentNegotiator {
  private final ContentNegotiationManager contentNegotiationManager;
  private final ContentTypeNegotiator delegate;

  /**
   * Construct a new {@link ContentNegotiator}.
   *
   * @param contentNegotiationManager the {@link ContentNegotiationManager}, autowired by Spring
   * @param supportedMediaTypes the media types, as {@link String}s, that this {@link
   *     ContentNegotiator} supports negotiation for
   */
  public ContentNegotiator(
      ContentNegotiationManager contentNegotiationManager, String... supportedMediaTypes) {
    this.contentNegotiationManager = contentNegotiationManager;
    this.delegate = buildContentTypeNegotiator(supportedMediaTypes);
  }

  /**
   * Perform negotiation, with configured supported media types, against a {@link WebRequest}.
   *
   * @param webRequest the {@link WebRequest} to perform negotiation against
   * @return the {@link MediaType} that is negotiated
   * @throws HttpMediaTypeNotAcceptableException if negotiation suceeds
   */
  public MediaType negotiate(WebRequest webRequest) throws HttpMediaTypeNotAcceptableException {
    List<MediaType> mediaTypes =
        contentNegotiationManager.resolveMediaTypes((NativeWebRequest) webRequest);
    List<me.jvt.http.mediatype.MediaType> converted = convert(mediaTypes);
    try {
      me.jvt.http.mediatype.MediaType negotiated = delegate.negotiate(converted);
      return MediaType.valueOf(negotiated.toString());
    } catch (NotAcceptableException e) {
      throw new HttpMediaTypeNotAcceptableException(e.getMessage());
    }
  }

  /**
   * Perform negotiation, with configured supported media types, against a {@link
   * HttpServletRequest}.
   *
   * @param httpServletRequest the {@link HttpServletRequest} to perform negotiation against
   * @return the {@link MediaType} that is negotiated
   * @throws HttpMediaTypeNotAcceptableException if negotiation suceeds
   */
  public MediaType negotiate(HttpServletRequest httpServletRequest)
      throws HttpMediaTypeNotAcceptableException {
    List<MediaType> mediaTypes =
        contentNegotiationManager.resolveMediaTypes(
            new DispatcherServletWebRequest(httpServletRequest));
    List<me.jvt.http.mediatype.MediaType> converted = convert(mediaTypes);
    try {
      me.jvt.http.mediatype.MediaType negotiated = delegate.negotiate(converted);
      return MediaType.valueOf(negotiated.toString());
    } catch (NotAcceptableException e) {
      throw new HttpMediaTypeNotAcceptableException(e.getMessage());
    }
  }

  private static List<me.jvt.http.mediatype.MediaType> convert(List<MediaType> mediaTypes) {
    return mediaTypes.stream()
        .map(me.jvt.http.mediatype.MediaType::from)
        .collect(Collectors.toList());
  }

  private static ContentTypeNegotiator buildContentTypeNegotiator(String... supportedMediaTypes) {
    return new ContentTypeNegotiator(parseAsMediaTypes(supportedMediaTypes));
  }

  private static List<me.jvt.http.mediatype.MediaType> parseAsMediaTypes(
      String[] supportedMediaTypes) {
    return Arrays.stream(supportedMediaTypes)
        .map(me.jvt.http.mediatype.MediaType::valueOf)
        .collect(Collectors.toList());
  }
}
